import {defineStore} from "pinia";

const url = 'https://api.platovcorp.site/product.json'
export const useStore = defineStore('shopStore', {
    state: () => ({
        activeNotice: false,
    }),
    actions: {
        setActiveTab() {
            this.activeNotice = true;
        },
    }
})